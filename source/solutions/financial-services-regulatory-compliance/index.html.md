---
layout: markdown_page
title: "Financial Services Regulatory Compliance"
---

## Introduction

GitLab is used extensively to achieve regulatory compliance in the financial services industry. Many of the world's largest financial institutions are GitLab customers.
This page details the relevant rules, the principles needed to achieve them, and the features in GitLab that make that possible.

## Regulators and regulations

Examples of regulators include the following
1. America: [FEB](https://www.federalreserve.gov/) in the US, also see the [FFIEC IT Handbook](https://ithandbook.ffiec.gov/)
1. Europe: [FCA](https://www.fca.org.uk/) and [PRA](https://www.bankofengland.co.uk/prudential-regulation) in the UK, [FINMA](https://www.finma.ch/en/) in Switzerland
1. Asia: [MAS](http://www.mas.gov.sg/)in Singapore and [HKMA](http://www.hkma.gov.hk/eng/index.shtml)

Examples of relevant regulations include the following
1. GLBA Safeguards rule requires that financial institutions must protect the consumer information they collect and hold service providers to same standards.
1. Dodd-Frank’s purpose is to promote the financial stability of the United States by improving accountability and transparency in the financial system. It sets the baseline for what is “reasonable and appropriate” security around consumer financial data. You must be ready to prove your security controls and document them.
1. Sarbanes Oxley (SOX) exists to protect investors by improving the accuracy and reliability of corporate disclosures made pursuant to the securities laws, and for other purposes.  Advice for achieving this is augmented by other frameworks such as COBIT14 and the CIS Critical Security Controls.
1. PCI DSS is intended to maintain payment security and is required for all entities that store, process or transmit cardholder data. It requires companies using credit cards to protect cardholder data,  manage vulnerabilities, provide strong access controls, monitor and test, and maintain policy.

Specific controls common amongst these regulations are outlined below, along with features of GitLab that aid in their compliance.

## Separation of duties

### Rules

TODO

### Principles

1. You never merge your own code.
1. All code needs to be peer reviewed.
1. Only authorized people can approve the code.
1. You need a log of who approved it

### Features

1. Protected branches https://docs.gitlab.com/ee/user/project/protected_branches.html
1. Merge request approvals https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html
1. Unprotect permission https://about.gitlab.com/2018/04/22/gitlab-10-7-released/#protected-branch-unprotect-permissions
1. Future: [Two-person access controls](https://gitlab.com/gitlab-org/gitlab-ee/issues/7176)

## Reverting

### Relevant rules

TODO

### Principles

1. https://en.wikipedia.org/wiki/2010_Flash_Crash
1. https://www.schneier.com/blog/archives/2018/04/tsb_bank_disast.html
1. Revert rollout fast

### Features

1. Automated deploy
1. Revert button

## Reviewing

### Relevant rules

TODO

### Principles


Review apps Great for testing algo changes.

## Deploys

1. Manual deploy (with audit log)
1. Approvers (in branch)

## Security

### Rules

TODO

### Principles

1. Scan applications regularly for vulnerabilities.
1. Establish criteria for the prioritization of vulnerabilities and remediation activities.
1. Pay special attention to internally or custom developed applications with dynamic and static analysis.
1. Establish secure coding as a culture, and provide qualified training on secure coding.
1. Establish and document a secure development life-cycle approach that fits your business and developers.
1. Combine functional testing and security testing of applications: Assess for operational bugs and coding errors.

## Features

1. SAST
1. DAST
1. Dependency scanning
1. Container scanning
1. Security Dashboard

Also see our [security paradigm](https://about.gitlab.com/handbook/product/#security-paradigm) for more information on why GitLab security tools work better.

## Auditing

1. One concept of a user across the lifecycle ensures the right level of permissions and access
1. Audit logs
1. Container image retention
1. Artifact retention
1. Test result retention
1. Future: Disable squash of commits
1. Future: Prevent purge

## Licensed code

1. License manager

## Disaster recovery

## Rules

## Principles

1. Stay available.
1. No SPOF
1. Quick recovery.
1. Geographic distribution

## Features

1. Geo
1. HA

## State of art

### Rules

TODO

### Principles

1. https://en.wikipedia.org/wiki/State_of_the_art#Tort_liability

## Features

1. GitLab is in use in financial services.
1. Both relevant US regulators run GitLab themselves.
1. 2,000 code contributors
1. 100,000 organizations
1. millions of users

## Interested

Contact sales

# Reference articles of interest

1. https://www.infoq.com/news/2016/07/devops-survival-finance
1. https://www.hpe.com/us/en/insights/articles/how-the-federal-reserve-bank-of-new-york-navigates-the-supply-chain-of-open-source-software-1710.html
1. https://www.hpe.com/us/en/insights/articles/primer-ensuring-regulatory-compliance-in-cloud-deployments-1704.html
1. https://www.sans.org/reading-room/whitepapers/analyst/understanding-security-regulations-financial-services-industry-37027


## Everyone can contribute

Please email suggestions

MRs are very welcome, assign to.

